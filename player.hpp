#include <iostream>
#include <string>
#include "vertex.h"

#define MOVE_INCREMENT .01
#define MAX_SPEED .03

using namespace std;

class Player{
	private:
		playerCoordinates *current;
		double posX, posY, rotation;
		double aX, aY, vX,vY;
		double relativeVerts[6][2];
		playerCoordinates *actualVerts;	
		double Hitbox[4];
	public:
	
	
		Player(){
			current = (playerCoordinates *) malloc(sizeof(playerCoordinates));
			posX = 0;
			posY = 0;
			aX = 0;
			aY = 0;
			vX = 0;
			vY = -.02;
			rotation = 0;
			relativeVerts[0][0] = 0;
			relativeVerts[0][1] = 0;
			relativeVerts[1][0] = -.03;
			relativeVerts[1][1] = -.08;
			relativeVerts[2][0] = 0;
			relativeVerts[2][1] = -.04;
			relativeVerts[3][0] = 0;
			relativeVerts[3][1] = 0;
			relativeVerts[4][0] = 0;
			relativeVerts[4][1] = -.04;
			relativeVerts[5][0] = .03;
			relativeVerts[5][1] = -.08;
			setVertices();
		}

		playerCoordinates *getVertices(){
			return current;
		}

                void setVertices(){
                        if(posX < -1.f){
                                posX = -1; 
                                posX *= -1;
                        } else if(posX>1.f){
                                posX = 1;
                                posX *= -1;
                        }
                        if(posY < -1.f || posY > 1.f){
                                if(posY<-1)
                                        posY = -1;
                                else if(posY > 1)
                                        posY = 1;
                                posY *= -1;
                        }
                        current->c1[0] = posX + relativeVerts[0][0];
                        current->c1[1] = posY + relativeVerts[0][1];
                        current->c2[0] = posX + relativeVerts[1][0];
                        current->c2[1] = posY + relativeVerts[1][1];
                        current->c3[0] = posX + relativeVerts[2][0];
                        current->c3[1] = posY + relativeVerts[2][1];
                        current->c4[0] = posX + relativeVerts[3][0];
                        current->c4[1] = posY + relativeVerts[3][1];
                        current->c5[0] = posX + relativeVerts[4][0];
                        current->c5[1] = posY + relativeVerts[4][1];
                        current->c6[0] = posX + relativeVerts[5][0];
                        current->c6[1] = posY + relativeVerts[5][1];
			calculateHitbox();
                }

		double getPosX(){
			return posX;
		}

		double getPosY(){
			return posY;
		}

		double getRotation(){
			return rotation;
		}

		void setRotation(double angle){
			rotation = angle;
		}

		void moveLeft(){
			aX = -1*MOVE_INCREMENT;
			vX += aX;
			if(vX < -1*MAX_SPEED)
				vX = -1*MAX_SPEED;
			posX += vX;
			posY += vY;
		}
		void moveRight(){
			aX = MOVE_INCREMENT;
			vX += aX;
			if(vX > MAX_SPEED)
				vX = MAX_SPEED;
			posX += vX;
			posY += vY;
			setVertices();
		}
		void moveUp(){
			aY = MOVE_INCREMENT;
			vY += aY;
			if(vY > MAX_SPEED)
				vY = MAX_SPEED;
			posX +=vX;
			posY +=vY;
			setVertices();

		}
		void moveDown(){
			aY = -1*MOVE_INCREMENT;
			vY += aY;
			if(vY < -1*MAX_SPEED)
				vY = -1*MAX_SPEED;
			posX+=vX;
			posY+=vY;
			setVertices();

		}
		void moveNone(){
			aX = 0;
			aY = 0;
			vX *= .97;
			vY *= .97;
			posX += vX;
			posY += vY;
			setVertices();
		}

		double* getHitbox(){
			return &Hitbox[0];
		}

		void calculateHitbox(){
			double minX, minY, maxX, maxY;
			minX = minY = 1.0;
			maxX = maxY = -1.0;
			int i = 0;
			int j = 0;
			double *c1 = current->c1;
			double *c2 = current->c2;
			double *c3 = current->c3;
			double *c4 = current->c4;
			double *c5 = current->c5;
			double *c6 = current->c6;
			//find minimum X
			if(minX > c1[0])
				minX = c1[0];
			if(minX > c2[0])
				minX = c2[0];
			if(minX > c3[0])
				minX = c3[0];
			if(minX > c4[0])
				minX = c4[0];
			if(minX > c5[0])
				minX = c5[0];
			if(minX > c6[0])
				minX = c6[0];
			//find maximum Y
			if(maxY < c1[1])
				maxY = c1[1];
			if(maxY < c2[1])
				maxY = c2[1];
			if(maxY < c3[1])
				maxY = c3[1];
			if(maxY < c4[1])
				maxY = c4[1];
			if(maxY < c5[1])
				maxY = c5[1];
			if(maxY < c6[1])
				maxY = c6[1];

			//find maximum X
			if(maxX < c1[0])
				maxX = c1[0];
			if(maxX < c2[0])
				maxX = c2[0];
			if(maxX < c3[0])
				maxX = c3[0];
			if(maxX < c4[0])
				maxX = c4[0];
			if(maxX < c5[0])
				maxX = c5[0];
			if(maxX < c6[0])
				maxX = c6[0];

			//find minumum Y
			if(minY > c1[1])
				minY = c1[1];
			if(minY > c2[1])
				minY = c2[1];
			if(minY > c3[1])
				minY = c3[1];
			if(minY > c4[1])
				minY = c4[1];
			if(minY > c5[1])
				minY = c5[1];
			if(minY > c6[1])
				minY = c6[1];

                Hitbox[0] = (maxX + minX)/2;//X of the center
                Hitbox[1] = (maxY + minY)/2;//Y of the center
//		printf("Player minX: %f, minY: %f, maxX: %f , maxY: %f\n",
//			minX, minY, maxX, maxY);
                Hitbox[2] = .02/2;
                Hitbox[3] = .03/2;


		}

		float getVel(){
			return sqrt(vX*vX+vY*vY);
		}
};
