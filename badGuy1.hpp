#include <iostream>
#include <string>
#include <time.h>
#include <math.h>
#define MOVE_INCREMENT .01
#define MAX_SPEED .02

using namespace std;

class BadGuy1{
	private:
		double posX, posY, rotation;
		double vX,vY;
		double relativeVerts[3][2];
		badGuy1Coordinates *verts;
		double Hitbox[4];
	public:
	
	
		BadGuy1(){
			verts = (badGuy1Coordinates *)malloc(sizeof(badGuy1Coordinates));
			if(verts == NULL) printf("NULL!");
			srand(time(0));
			vX = rand()%100+1;
			vY = rand()%100+1;
			double speedFactor = (vX * vX+vY*vY)*1000;
			speedFactor = sqrt(speedFactor);
			vX /= speedFactor;
			vY /= speedFactor;
			randomizeRelatives();
			setVerts();
		}

		void randomizeRelatives(){
			int seedX = rand()%200; 
			int seedY = rand()%200; 
			float seedX_f = seedX/100.f-1;
			float seedY_f = seedY/100.f-1;
			posX = 0.0f;
			posY = 0.f;
			//posX = seedX_f;
			//posY = seedY_f;
//			printf("SeedX: %d SeedY: %d\n", seedX, seedY);
//			printf("SeedX_f: %f SeedY_f: %f\n", seedX_f, seedY_f);
			relativeVerts[0][0] = 0.f;
			relativeVerts[0][1] =  0.f;
			relativeVerts[1][0] =  -.05f;
			relativeVerts[1][1] =  -.05f;
			relativeVerts[2][0] = -.05f;
			relativeVerts[2][1] = .05f;
		}

		void setVerts(){
			verts->c1[0] = relativeVerts[0][0] + posX;
			verts->c1[1] = relativeVerts[0][1] + posY;
			verts->c2[0] = relativeVerts[1][0] + posX;
			verts->c2[1] = relativeVerts[1][1] + posY;
			verts->c3[0] = relativeVerts[2][0] + posX;
			verts->c3[1] = relativeVerts[2][1] + posY;
			calculateHitbox();
		}
		double getPositionx(){
			return posX;
		}
		double getPositiony(){
			return posY;
		}
		badGuy1Coordinates *getVertices(){
			
			return verts;
		}

		void update(){
			if(posX <= -1){
				vX *= -1;
				posX = -.9999f;
			}
			else if(posX >= 1){
				vX *= -1;
				posX = .9999f;
			
			}
			if(posY <= -1){
				vY *= -1;
				posY = -.9999f;
			}
			else if(posY >= 1){
				vY *= -1;
				posY = .9999f;
			}
			posX +=vX;
			posY +=vY;
		}

		double* getHitbox(){
                        return &Hitbox[0];
                }

                void calculateHitbox(){
                        double minX, minY, maxX, maxY;
                        minX = minY = 1.0; 
			maxX = maxY = -1.0;
                        int i = 0;
                        int j = 0;
                        double *c1 = verts->c1;
                        double *c2 = verts->c2;
                        double *c3 = verts->c3;
                        //find minimum X
                        if(minX > c1[0])
                                minX = c1[0];
                        if(minX > c2[0])
                                minX = c2[0];
                        if(minX > c3[0])
                                minX = c3[0];
                        //find maximum Y
                        if(maxY < c1[1])
                                maxY = c1[1];
                        if(maxY < c2[1])
                                maxY = c2[1];
                        if(maxY < c3[1])
                                maxY = c3[1];

                        //find maximum X
                        if(maxX < c1[0])
                                maxX = c1[0];
                        if(maxX < c2[0])
                                maxX = c2[0];
                        if(maxX < c3[0])
                                maxX = c3[0];

                        //find minumum Y
                        if(minY > c1[1])
                                minY = c1[1];
                        if(minY > c2[1])
                                minY = c2[1];
                        if(minY > c3[1])
                                minY = c3[1];

	                Hitbox[0] = (maxX + minX)/2;//X of the center
	                Hitbox[1] = (maxY + minY)/2;//Y of the center
	                Hitbox[2] = .04/2;
	                Hitbox[3] = .04/2;//Y radius
		}
};
