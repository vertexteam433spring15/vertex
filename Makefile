#
#	Makefile for OpenGL example from Chapter 1 in Red Book
#

CC = g++ 
CFLAGS = -D_DEBUG
CPPFLAGS = -g

.cpp.o:
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@  $<

LIBS = -lGL -lGLU -lGLEW -lglut

SRCS = vertex.cpp LoadShaders.cpp
OBJS = vertex.o LoadShaders.o

example1: $(OBJS) LoadShaders.h player.hpp badGuy1.hpp
	g++ -g -o vertex $(OBJS) $(LIBS)

clean:
	rm -f vertex *.o
