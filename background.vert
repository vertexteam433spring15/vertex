#version 430 core

layout(location = 0) in vec4 vPosition;
out float x;
out float y;
void
main()
{
    x = vPosition.x;
    y = vPosition.y;
    gl_Position =  vPosition;
}
