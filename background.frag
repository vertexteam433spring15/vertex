#version 430 core
out vec4 fColor;
in float x;
in float y;
uniform float time;
uniform float playerX;
uniform float playerY;
uniform float playerSpeed;
uniform float playerRotation;
float t1 = 5.0f;
float t2 = 15.0f;
float t3 = 25.0f;
float t4 = 40.0f;
void
main()
{
    	if(time < t1){
		fColor = (time/t1)*vec4(abs(x+y)/2,abs(x),abs(y),0.0);	
	}
	else if(time < t2){	
		fColor = vec4(abs((time-t2)/(t2-t1))*(abs((x+y)/2)*(.5f*cos(.9f*(time-t1))+.5f))+((time-t1)/(t2-t1))*(0.5f*sin(4*x*y)+0.5f),(abs((time-t2)/(t2-t1))*abs(x)*(.5f*cos(.7f*(time-t1))+.5f))+((time-t1)/(t2-t1))*(x*y),(abs((time-t2)/(t2-t1))*abs(y)*(cos(1.3f*(time-t1))*.5f+.5f))+((time-t1)/(t2-t1))*(0.5f*cos(x*y*(time-t1-15))+.5),0.0);
		}
	else if(time < t3){
		fColor = vec4(abs((time-t3)/(t3-t2))*(0.5f*sin(4*x*y)+0.5f)+((time-t2)/(t3-t2))*(.5f*sin(((x*x)/(y*x))*(time-t2-10))+.5f),abs((time-t3)/(t3-t2))*(x*y)+((time-t2)/(t3-t2))*(.5f*cos(((y*y)/(y*x))*(time-t2-10))+.5f),(abs((time-t3)/(t3-t2))*0.5f*cos(x*y*(time-t1-15))+.5)+((time-t2)/(t3-t2))*(sin(atan(x/y)*x*x*y*x*(time-t2-10))),0.0);
		}
	else if(time < t4){
		fColor = vec4(abs((time-t4)/(t4-t3))*(.5f*sin(((x*x)/(y*x))*(time-t2-10))+.5f)+((time-t2)/(t3-t2))*(abs(cos(sin(sqrt(x*x+y*y)*(time-t3-15))*sin(sqrt(x*x+y*y)*(time-t3-15))))), abs((time-t4)/(t4-t3))*(.5f*cos(((y*y)/(y*x))*(time-t2-10))+.5f)+((time-t2)/(t3-t2))*(abs(cos(atan(x/y)*(time-t3-15)))),abs((time-t4)/(t4-t3))*(sin(atan(x/y)*x*x*y*x*(time-t2-10)))+((time-t2)/(t3-t2))*(abs(cos(sin(y)/sin(x)*(time-t3-15))*cos((sin(y)/sin(x)*(time-t3-15))))),0.0f);
	}
	else{
	fColor = vec4((abs(cos(sin(sqrt(x*x+y*y)*(time-t3-15))*sin(sqrt(x*x+y*y)*(time-t3-15))))),(abs(cos(atan(x/y)*(time-t3-15)))),(abs(cos(sin(y)/sin(x)*(time-t3-15))*cos((sin(y)/sin(x)*(time-t3-15))))), 0.0 );
	}
	//if(x-playerX < .075 && x-playerX > -.075 && y-playerY < .075 &&y-playerY > -.075){
		fColor += .5-sqrt((x-playerX)*(x-playerX)+(y-playerY)*(y-playerY));
	//}
}
