#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "vgl.h"
#include "LoadShaders.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <math.h>
#include <time.h>
#include <string>
#include <sstream>
#include <vector>

//class files
#include "player.hpp"
#include "badGuy1.hpp"
#include "badGuy2.hpp"
//#define M_PI 3.141592653589323846
#define MAX_BULLETS 5
#define MAX_ENEMIES 100
#define WINDOW_HEIGHT 512
#define WINDOW_LENGTH 512
#define ESC 27
#define DEBUG_MOVEMENT 0
#define DEBUG 0

using namespace std;
using namespace glm;

GLuint PlayerArrayID, EnemyArrayID, Enemy2ArrayID, BackgroundID;
GLuint enemy1buffer, enemy2buffer;
GLuint programPlayer, programBullets, programEnemies, programEnemies2, programBackground;
float mouse_x,mouse_y;
Player player;
std::vector <BadGuy1> enemy;
vector <BadGuy2> enemy2;
mat4 playerRotation;
mat4 playerTranslation;
GLuint playerRotationID;
vector <glm::vec2> enemyVertices;
vector <glm::vec2> enemy2Vertices;
GLuint playerTranslationID;

//for player shader
float myTime = 0.f;
int timeID;
float vel = 0.f;
int velID;
float rotation = 1.f;
int rotationID;
int global_height;
int global_width;
int numEnemies;
int numEnemies2;
int gameLoops = 0;

int playerXID;
int playerYID;

bool playerIsDead = false;
int deadPlayerFragments = 0;
float deadPlayerTriangles [15][2];

int test;
int theta;
int timeMarker;
int mouseTimeMarker;

void updatePlayer(void);
bool checkCollisions(double hitbox1[], double hitbox2[]);
void myReshapeFunc(int width, int height);
/////////////////////////////////////////////////////////////////////
///
/// Initializes everything needed for the game: Generates enemies,
/// loads shaders, 
///
////////////////////////////////////////////////////////////////////
void init(){
	theta = 0;
	numEnemies = 0;
	numEnemies2 = 0;

//	enemy.push_back(BadGuy1());
//	enemy2.push_back(BadGuy2());
	/*
	for(int i =0; i < 3; i++){
	  	sleep(1);
		enemy.push_back(BadGuy1());
		numEnemies++;
		}*/

	playerCoordinates* pc = player.getVertices();
	badGuy1Coordinates* enemyCoordinates;
	glm::vec2 wholeScreen[6];
/*	for(int i = 0; i < 1; i++){
		printf("Initial player information:\n");
		printf("C1[0]: %f [1]: %f\n", pc->c1[i], pc->c1[i+1]);
		printf("C2[0]: %f [1]: %f\n", pc->c2[i], pc->c2[i+1]);
		printf("C3[0]: %f [1]: %f\n", pc->c3[i], pc->c3[i+1]);
		printf("C4[0]: %f [1]: %f\n", pc->c4[i], pc->c4[i+1]);
		printf("C5[0]: %f [1]: %f\n", pc->c5[i], pc->c5[i+1]);
		printf("C6[0]: %f [1]: %f\n", pc->c6[i], pc->c6[i+1]);
	}
*/
	/*
	 * Specifying shaders
	 */ 

	ShaderInfo basicShader[] = {
	  { GL_VERTEX_SHADER, "motionBlur.vert" },
	  { GL_FRAGMENT_SHADER, "motionBlur.frag" },
	  { GL_NONE, NULL }
	};

        ShaderInfo enemyShader[] = {
          { GL_VERTEX_SHADER, "enemy1.vert" },
          { GL_FRAGMENT_SHADER, "enemy1.frag" },
          { GL_NONE, NULL }
        };

        ShaderInfo enemyShader2[] = {
          { GL_VERTEX_SHADER, "enemy2.vert" },
          { GL_FRAGMENT_SHADER, "enemy2.frag" },
          { GL_NONE, NULL }
        };


	ShaderInfo backgroundShader[] = {
		{ GL_VERTEX_SHADER, "background.vert" },
		{ GL_FRAGMENT_SHADER, "background.frag" },
		{ GL_NONE, NULL}
	};

	/*
	 * Loading shaders
	 */

	programPlayer = LoadShaders( basicShader );
	programEnemies = LoadShaders( enemyShader );
	programEnemies2 = LoadShaders( enemyShader2 );
	programBackground = LoadShaders( backgroundShader );

	glUseProgram(programBackground);
	glGenVertexArrays( 1, &BackgroundID);
	glBindVertexArray(BackgroundID);

	wholeScreen[0].x = -1.0f;
	wholeScreen[0].y = 1.0f;
	wholeScreen[1].x = -1.0f;
	wholeScreen[1].y = -1.0f;
	wholeScreen[2].x = 1.0f;
	wholeScreen[2].y = -1.0f;
	wholeScreen[3].x = -1.0f;
	wholeScreen[3].y = 1.0f;
	wholeScreen[4].x = 1.0f;
	wholeScreen[4].y = 1.0f;
	wholeScreen[5].x = 1.0f;
	wholeScreen[5].y = -1.0f;

	GLuint backBuffer;
	glGenBuffers( 1, &backBuffer);
	glBindBuffer( GL_ARRAY_BUFFER, backBuffer);
	glBufferData( GL_ARRAY_BUFFER, 6 *sizeof(glm::vec2), &wholeScreen[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(glm::vec2), 0);
	glEnableVertexAttribArray(0);

	glUseProgram( programPlayer );
	mat4 playerRotation = mat4(1.f);

	glGenVertexArrays( 1, &PlayerArrayID );
	glBindVertexArray( PlayerArrayID );
	
	/*
	 * Specifying the 6 vertices that make up the player ship
	 */ 

	glm::vec2 vertex1 = glm::vec2(pc->c1[0], pc->c1[1]);
	glm::vec2 vertex2 = glm::vec2(pc->c2[0], pc->c2[1]);
	glm::vec2 vertex3 = glm::vec2(pc->c3[0], pc->c3[1]);
	glm::vec2 vertex4 = glm::vec2(pc->c4[0], pc->c4[1]);
	glm::vec2 vertex5 = glm::vec2(pc->c5[0], pc->c5[1]);
	glm::vec2 vertex6 = glm::vec2(pc->c6[0], pc->c6[1]);


	vector<glm::vec2> vertices;
	//First player ship triangle
	vertices.push_back(vertex1); vertices.push_back(vertex2); vertices.push_back(vertex3);
	//Second player ship triangle
	vertices.push_back(vertex4); vertices.push_back(vertex5); vertices.push_back(vertex6);

	GLuint new_buffer;

	glGenBuffers( 1, &new_buffer);
	glBindBuffer( GL_ARRAY_BUFFER, new_buffer );
	glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer( 0, //Index of generic vertex attribute to be modified
			       2, //Size: number of components to read per vertex attribute. 3 for triangles
			       GL_FLOAT, //type: data type of each component in the Vertex Array.
			       GL_FALSE, //Should this be normalized? No.
			       sizeof(glm::vec2), //Stride: byte offset between consecutive vertex attributes. 0, because it is a tightly packed array
			       0
			       );

	glEnableVertexAttribArray( 0 );
			       
        glGenVertexArrays( 1, &EnemyArrayID );
        glBindVertexArray( EnemyArrayID );

	int i;
	for(i = 0; i < numEnemies; i++){
		enemyCoordinates = enemy[i].getVertices();
	        enemyVertices.push_back(glm::vec2(enemyCoordinates->c1[0],enemyCoordinates->c1[1]));
	        enemyVertices.push_back(glm::vec2(enemyCoordinates->c2[0],enemyCoordinates->c2[1]));
	        enemyVertices.push_back( glm::vec2(enemyCoordinates->c3[0],enemyCoordinates->c3[1]));
	}

 	enemy2.push_back(BadGuy2());
        enemyCoordinates = enemy2[0].getVertices();
        enemy2Vertices.push_back(glm::vec2(enemyCoordinates->c1[0],enemyCoordinates->c1[1]));
        enemy2Vertices.push_back(glm::vec2(enemyCoordinates->c2[0],enemyCoordinates->c2[1]));
        enemy2Vertices.push_back( glm::vec2(enemyCoordinates->c3[0],enemyCoordinates->c3[1]));


        glGenBuffers( 1, &enemy1buffer);
        glBindBuffer( GL_ARRAY_BUFFER, enemy1buffer );
        glBufferData( GL_ARRAY_BUFFER, sizeof(enemyVertices) * sizeof(glm::vec2), enemyVertices.data(), GL_DYNAMIC_DRAW);
        glVertexAttribPointer( 0, //Index of generic vertex attribute to be modified
                               2, //Size: number of components to read per vertex attribute. 3 for triangles
                               GL_FLOAT, //type: data type of each component in the Vertex Array.
                               GL_FALSE, //Should this be normalized? No.
                               0,//sizeof(glm::vec2), //Stride: byte offset between consecutive vertex attributes. 0, because it is a tightly packed array
                               0
                               );

        glEnableVertexAttribArray( 0 );

        glGenVertexArrays( 1, &Enemy2ArrayID );
 	glGenBuffers( 1, &enemy2buffer);
        glBindBuffer( GL_ARRAY_BUFFER, enemy2buffer );
        glBufferData( GL_ARRAY_BUFFER, sizeof(enemy2Vertices) * sizeof(glm::vec2), enemy2Vertices.data(), GL_DYNAMIC_DRAW);
        glVertexAttribPointer( 0, //Index of generic vertex attribute to be modified
                               2, //Size: number of components to read per vertex attribute. 3 for triangles
                               GL_FLOAT, //type: data type of each component in the Vertex Array.
                               GL_FALSE, //Should this be normalized? No.
                               0,//sizeof(glm::vec2), //Stride: byte offset between consecutive vertex attributes. 0, because it is a tightly packed array
                               0
                               );
        glEnableVertexAttribArray( 0 );

}

////////////////////////////////////////////////////////////////////
// keyboard 
// --code adapted from zeuscmd.com/tutorials/glut
////////////////////////////////////////////////////////////////////
void keyboard(unsigned char key, int x, int y){
	if (key == ESC){
	   exit(0);
	}
	if(playerIsDead)
		return;
	playerCoordinates* pc = player.getVertices();
	switch (key){

		case 'w':
		case 'W':
			//move the player ship upwards
			player.moveUp();
			if(DEBUG)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		case 's':
		case 'S':
			//move the player ship downwards
			player.moveDown();
			if(DEBUG)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			break;
		case 'a':
		case 'A':
			//move the player ship left
			player.moveLeft();
			break;
		case 'd':
		case 'D':
			//move the player ship right
			player.moveRight();
			break;
		case 'p':
			if(DEBUG)
        		for(int i = 0; i < 1; i++){
                		printf("C1[0]: %f [1]: %f\n", pc->c1[i], pc->c1[i+1]);
                		printf("C2[0]: %f [1]: %f\n", pc->c2[i], pc->c2[i+1]);
                		printf("C3[0]: %f [1]: %f\n", pc->c3[i], pc->c3[i+1]);
                		printf("C4[0]: %f [1]: %f\n", pc->c4[i], pc->c4[i+1]);
                		printf("C5[0]: %f [1]: %f\n", pc->c5[i], pc->c5[i+1]);
               	 		printf("C6[0]: %f [1]: %f\n", pc->c6[i], pc->c6[i+1]);
		        }

			break;
		case ESC:
			//escape or quit the application
			exit(0);
			break;
		default:
			//ignore any other keypress
			break;
	}
	if(!playerIsDead){
		player.setVertices();
		pc = player.getVertices();
	}
	if(DEBUG_MOVEMENT){
        for(int i = 0; i < 1; i++){
                printf("C1[0]: %f [1]: %f\n", pc->c1[i], pc->c1[i+1]);
                printf("C2[0]: %f [1]: %f\n", pc->c2[i], pc->c2[i+1]);
                printf("C3[0]: %f [1]: %f\n", pc->c3[i], pc->c3[i+1]);
                printf("C4[0]: %f [1]: %f\n", pc->c4[i], pc->c4[i+1]);
                printf("C5[0]: %f [1]: %f\n", pc->c5[i], pc->c5[i+1]);
                printf("C6[0]: %f [1]: %f\n", pc->c6[i], pc->c6[i+1]);
        }
	}
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////
//
//	display
//
//
////////////////////////////////////////////////////////////////////

void display(void)
{
        int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
        if((timeSinceStart - timeMarker) >= 1000/30){ 
		gameLoops++;
                timeMarker = timeSinceStart;
		myTime += .01f;

		if(gameLoops%600 == 0){
                        enemy2.push_back(BadGuy2(numEnemies2*.1));
                        badGuy1Coordinates* enemyCoordinates = enemy2[numEnemies2].getVertices();
                        enemy2Vertices.push_back(glm::vec2(enemyCoordinates->c1[0],enemyCoordinates->c1[1]));
                        enemy2Vertices.push_back(glm::vec2(enemyCoordinates->c2[0],enemyCoordinates->c2[1]));
                        enemy2Vertices.push_back( glm::vec2(enemyCoordinates->c3[0],enemyCoordinates->c3[1]));
                        numEnemies2++;
                        glBindVertexArray(Enemy2ArrayID);

                        glBindBuffer( GL_ARRAY_BUFFER, enemy2buffer );
                        glBufferData( GL_ARRAY_BUFFER, sizeof(enemy2Vertices) * sizeof(glm::vec2), enemy2Vertices.data(), GL_DYNAMIC_DRAW);
                        glVertexAttribPointer( 0, //Index of generic vertex attribute to be modified
                               2, //Size: number of components to read per vertex attribute. 3 for triangles
                               GL_FLOAT, //type: data type of each component in the Vertex Array.
                               GL_FALSE, //Should this be normalized? No.
                               sizeof(glm::vec2), //Stride: byte offset between consecutive vertex attributes. 0, because it is a tightly packed$
                               0
                               );

                        glEnableVertexAttribArray( 0 );

		}
                else if(gameLoops%200 == 0){
                  //Creating a new enemy
                        if(numEnemies < 8){
                        enemy.push_back(BadGuy1());
                        badGuy1Coordinates* enemyCoordinates = enemy[numEnemies].getVertices();
                        enemyVertices.push_back(glm::vec2(enemyCoordinates->c1[0],enemyCoordinates->c1[1]));
                        enemyVertices.push_back(glm::vec2(enemyCoordinates->c2[0],enemyCoordinates->c2[1]));
                        enemyVertices.push_back( glm::vec2(enemyCoordinates->c3[0],enemyCoordinates->c3[1]));
                        numEnemies++;
                        glBindVertexArray(EnemyArrayID);

                        glBindBuffer( GL_ARRAY_BUFFER, enemy1buffer );
                        glBufferData( GL_ARRAY_BUFFER, sizeof(enemyVertices) * sizeof(glm::vec2), enemyVertices.data(), GL_DYNAMIC_DRAW);
                        glVertexAttribPointer( 0, //Index of generic vertex attribute to be modified
                               2, //Size: number of components to read per vertex attribute. 3 for triangles
                               GL_FLOAT, //type: data type of each component in the Vertex Array.
                               GL_FALSE, //Should this be normalized? No.
                               sizeof(glm::vec2), //Stride: byte offset between consecutive vertex attributes. 0, because it is a tightly packed array
                               0
                               );

                        glEnableVertexAttribArray( 0 );
                      }
                }
		if(!playerIsDead)
			player.moveNone();
//		updatePlayer();

		double xDiff = mouse_x - player.getPosX();
		double yDiff = mouse_y - player.getPosY();
        	double angle = atan2(yDiff, xDiff);
		rotation = angle;

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	        //printf("Angle to the mouse: %f\n", angle);
	        player.setRotation(angle);
//     		printf("Angle to the mouse: %f\n", player.getRotation());
        	playerRotation = glm::rotate(mat4(1.f), float(angle- M_PI/2), vec3(0,0,1));
		playerTranslation = glm::translate(mat4(1.f), vec3(player.getPosX(), player.getPosY(), 0));
        	playerRotationID = glGetUniformLocation(programPlayer, "rotation");

	        playerTranslationID = glGetUniformLocation(programPlayer, "translation");
	        glProgramUniformMatrix4fv(programPlayer, playerRotationID, 1, GL_FALSE, &playerRotation[0][0]);
		glProgramUniformMatrix4fv(programPlayer, playerTranslationID, 1, GL_FALSE, &playerTranslation[0][0]);
	//        glutPostRedisplay();
	
		timeID = glGetUniformLocation(programBackground, "time");
		glProgramUniform1f(programBackground,timeID,myTime);
		playerXID = glGetUniformLocation(programBackground, "playerX");
		glProgramUniform1f(programBackground,playerXID,player.getPosX());
		playerYID = glGetUniformLocation(programBackground, "playerY");
		glProgramUniform1f(programBackground,playerYID,player.getPosY());
                timeID = glGetUniformLocation(programPlayer, "time");
                glProgramUniform1f(programPlayer,timeID,myTime);

		rotationID = glGetUniformLocation(programPlayer, "playerRotation");
		glProgramUniform1f(programPlayer, rotationID, rotation);
	
		velID = glGetUniformLocation(programPlayer, "vel");
		vel = player.getVel();
		glProgramUniform1f(programPlayer, velID, vel);
	
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		int i;
		glBindVertexArray(BackgroundID);
		glUseProgram( programBackground );
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glLineWidth(3.f);
		//draw the player ship
		glUseProgram( programPlayer );

		if(!playerIsDead){
			glBindVertexArray(PlayerArrayID);
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}
		else if(playerIsDead){
			
			//glDrawArrays(GL_TRIANGLES,0,deadPlayerFragments);
		}
		//glDrawArrays(GL_TRIANGLES,6,9);
		
		//draw the bullets
		glUseProgram(programBullets);
		for(i = 0; i < MAX_BULLETS; i++){
	
		}

		//draw the enemy ships
		glBindVertexArray(EnemyArrayID);
		glUseProgram(programEnemies);
		for(i = 0; i < numEnemies; i++){
		    if(!playerIsDead){
  		    enemy[i].update();
	                enemy[i].setVerts();
		    }
	                checkCollisions(player.getHitbox(), enemy[i].getHitbox());

	                mat4 enemyTranslation = glm::translate(mat4(1.f), vec3(enemy[i].getPositionx(), enemy[i].getPositiony(), 0));
	                int enemyTranslationID = glGetUniformLocation(programEnemies, "translation");
	                glProgramUniformMatrix4fv(programEnemies, enemyTranslationID, 1, GL_FALSE, &enemyTranslation[0][0]);
			glDrawArrays(GL_TRIANGLES, i*3, 3);

		}

		glBindVertexArray(Enemy2ArrayID);
		glUseProgram(programEnemies2);
                for(i = 0; i < numEnemies2; i++){
	             if(!playerIsDead){
			   enemy2[i].update(player.getPosX(), player.getPosY());
                           enemy2[i].setVerts();
		    }

                        checkCollisions(player.getHitbox(), enemy2[i].getHitbox());

                        mat4 enemy2Translation = glm::translate(mat4(1.f), vec3(enemy2[i].getPositionx(), enemy2[i].getPositiony(), 0));
                        int enemy2TranslationID = glGetUniformLocation(programEnemies2, "translation");
                        glProgramUniformMatrix4fv(programEnemies2, enemy2TranslationID, 1, GL_FALSE, &enemy2Translation[0][0]);

			mat4 enemy2Rotation = glm::rotate(mat4(1.f), float(enemy2[i].getAngle()), vec3(0,0,1));
		 	GLint enemy2RotationID = glGetUniformLocation(programEnemies2, "rotation");
                	glProgramUniformMatrix4fv(programEnemies2, enemy2RotationID, 1, GL_FALSE, &enemy2Rotation[0][0]);

                        glDrawArrays(GL_TRIANGLES, i*3, 3);

                }


		//glFlush();
		glutSwapBuffers();
		glutPostRedisplay();
	}
}

bool checkCollisions(double hitbox1[], double hitbox2[]){
	if(playerIsDead){
		return false;
	}
	//(CenterX1 - CenterX2) > (sum of the X radius)
	//won't get collision there
	if (abs(hitbox1[0] - hitbox2[0]) > (hitbox1[2] + hitbox2[2])) return false;

	//(CenterY1 - CenterY2) > (sum of the Y radius)
	//won't get collision there
	if (abs(hitbox1[1] - hitbox2[1]) > (hitbox1[3] + hitbox2[3])) return false;

//	printf("OH EM GEE COLLISIONS!!!\n");
//	printf("Player Hitbox Center: ( %4f, %4f) Radius: ( %4f, %4f)\n", hitbox1[0], hitbox1[1],
//									  hitbox1[2], hitbox1[3]);
//	printf("Enemy Hitbox Center: ( %4f, %4f) Radius: ( %4f, %4f)\n",  hitbox2[0], hitbox2[1],
//                                                                          hitbox2[2], hitbox2[3]);
	playerIsDead = true;
	printf("Congratulations! You survived %f seconds!\n", ((float)gameLoops)*30/1000);
	deadPlayerFragments = 15;
	for(int i=0;i<deadPlayerFragments;i++){

	}
	return true;
}

////////////////////////////////////////////////////////////////////////
//	mouseLocation
////////////////////////////////////////////////////////////////////////
void mouseLocation(int x, int y){
        //int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
        //if((timeSinceStart - mouseTimeMarker) >= 1000/30){ 
        //        mouseTimeMarker = timeSinceStart;

		mouse_x = ((float) x - (global_width)/2)/(global_width/2);
		mouse_y = (((float)y - (global_height)/2)/(global_height/2)) *-1;
		//printf("Mouse X: %f\n", mouse_x);
		//printf("Mouse Y: %f\n", mouse_y);
		//track the mouse by getting the angle from the ship to
		//the mouse
		double xDiff = mouse_x - player.getPosX();
		double yDiff = mouse_y - player.getPosY();	

		double angle = atan2(yDiff, xDiff);
		rotation = (float)angle;
		//printf("Angle to the mouse: %f\n", angle);
		player.setRotation(angle);
//		printf("Angle to the mouse: %f\n", player.getRotation());
		playerRotation = glm::rotate(mat4(1.f), float(angle- M_PI/2), vec3(0,0,1));
		playerRotationID = glGetUniformLocation(programPlayer, "rotation");
		glProgramUniformMatrix4fv(programPlayer, playerRotationID, 1, GL_FALSE, &playerRotation[0][0]);
		theta = 0;
		glutPostRedisplay();
	//}

}

//////////////////////////////////////////////////////////////////////
//
//	mouseClick
//
/////////////////////////////////////////////////////////////////////
void mouseClick(int button, int state, int x, int y){
	float convertedX, convertedY;
	convertedX = ((float) x - (global_width)/2)/(global_width/2);
	convertedY = (((float)y - (global_height)/2)/(global_height/2)) *-1;
	switch(button){
		case GLUT_LEFT_BUTTON:
		/*If we have time for a charge shot we can track the
		 *charge amount in the MOUSE_DOWN state and then fire
		 *when its released*/
			if(state == GLUT_DOWN){
//				printf("Mouse clicked at X: %f Y: %f\n",
//					convertedX, convertedY);
			}
			break;
	}
}

////////////////////////////////////////////////////////////////////////
//
//	myIdleFunc
//		its what comes out of my car at a stoplight
//
////////////////////////////////////////////////////////////////////////

void myIdleFunc(void){
//        int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
//	if((timeSinceStart - timeMarker) >= 1000/30){ 
//        	timeMarker = timeSinceStart;
		glutPostRedisplay();
//	}
}

void myReshapeFunc(int width, int height){
	global_width = width;
	global_height = height;
	glViewport(0, 0, width, height);
}

////////////////////////////////////////////////////////////////////////
//	main
////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
	timeMarker = 0;
	glutInit( &argc, argv );
	glutInitDisplayMode(GLUT_DOUBLE| GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize( WINDOW_HEIGHT, WINDOW_LENGTH );
	glutInitContextVersion( 4, 3 );
	glutInitContextProfile( GLUT_CORE_PROFILE );// GLUT_COMPATIBILITY_PROFILE );
	global_width = WINDOW_LENGTH;
	global_height = WINDOW_HEIGHT;
	glutCreateWindow( argv[0] );

	glewExperimental = GL_TRUE;	// added for glew to work!
	if ( glewInit() )
	{
		cerr << "Unable to initialize GLEW ... exiting" << endl;
		exit (EXIT_FAILURE );
	}

	//
	//GLint nExtensions;
	//glGetIntegerv( GL_NUM_EXTENSIONS, &nExtensions );
	//for ( int i = 0; i < nExtensions; i++ )
	//	cout << glGetStringi( GL_EXTENSIONS, i )  << endl;

	init();
	glutKeyboardFunc(keyboard);
	glutPassiveMotionFunc(mouseLocation);
	glutMouseFunc(mouseClick);
	glutDisplayFunc( display );
	glutReshapeFunc( myReshapeFunc );
	glutIdleFunc( myIdleFunc );
	glutMainLoop();

	return 0;
}

