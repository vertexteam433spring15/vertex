#version 430 core

layout(location = 0) in vec4 vPosition;

uniform mat4 rotation;
uniform mat4 translation;
uniform float time;
float t1 = 10.0f;
vec4 position;
void
main()
{
        gl_Position =  translation * rotation * vPosition;
	//if(time > t1){
	//	gl_Position.x += .03*sin(3*(time-t1));
	//	gl_Position.y += .03*cos(5*(time-t1));
    //}
}
