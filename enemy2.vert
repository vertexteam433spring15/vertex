#version 430 core

layout(location = 0) in vec4 vPosition;
uniform mat4 translation;
uniform mat4 rotation;
out vec4 Position;
void
main()
{
    gl_Position = translation * rotation * vPosition;
    Position = vPosition;

}
