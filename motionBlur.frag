#version 430 core
out vec4 fColor;
uniform float time;
uniform float vel;
uniform float shipRotation;
void
main()
{
    fColor = vec4( 0.5f*sin(time*1.3)+0.5f, 0.5f*sin(vel*1.7f)+0.5, 0.5f*sin(time*1.9f)+0.5f, 0.0 );
}
